const express = require('express')
const router = express.Router()

const StudyPlan = require('../models/studyplan.model')();

const CrudController = require('../controllers/crud.js');
const StudyPlanCrudController = new CrudController(StudyPlan);
const StudyPlanController = require('../controllers/studyplan.controller');

// get all studyplans from user
router.get('/users/:userId/studyplans', StudyPlanController.findAllFromUser)

// get a studyplan from user by Id
router.get('/studyplans/:id', StudyPlanCrudController.getOne)

// add studyplan to user
router.post("/users/:userId/studyplans", StudyPlanController.create)

// delete studyplan from user
router.delete("/users/:userId/studyplans/:id", StudyPlanController.remove)

// update studyplan from user
router.put("/studyplans/:id", StudyPlanCrudController.update)

module.exports = router