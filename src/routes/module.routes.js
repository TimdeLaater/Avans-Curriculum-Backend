const express = require('express')
const router = express.Router()

const Module = require('../models/module.model.js')();

const CrudController = require('../controllers/crud.js');

const ModuleCrudController = new CrudController(Module);

// get all modules
router.get('/modules', ModuleCrudController.getAll);

// get a module by Id
router.get('/modules/:id', ModuleCrudController.getOne);

// add a module
router.post("/modules", ModuleCrudController.create);

// delete a module
router.delete("/modules/:id", ModuleCrudController.update);

// update a module
router.put("/modules/:id", ModuleCrudController.update);

module.exports = router