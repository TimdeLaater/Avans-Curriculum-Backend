const express = require('express');
const router = express.Router();

const Certificiate = require('../models/certificate.model')();

const CrudController = require('../controllers/crud.js');
const CertificateCrudController = new CrudController(Certificiate);
const CertificateController = require('../controllers/certificate.controller');

// get all certificates
router.get('/certificates', CertificateCrudController.getAll);
//post certificate
router.post('/certificates', CertificateCrudController.create);

// get a certificates by Id
router.get('/certificates/:id', CertificateCrudController.getOne);

// add certificate to user by userId
router.post("/users/:userId/certificates/:id", CertificateController.create);

// get certificates from user by userId 
router.get('/users/:userId/certificates', CertificateController.findAllFromUser);

module.exports = router