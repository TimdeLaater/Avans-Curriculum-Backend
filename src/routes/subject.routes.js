const express = require('express');
const router = express.Router();

const Subject = require('../models/subject.model')();

const CrudController = require('../controllers/crud.js');
const SubjectCrudController = new CrudController(Subject);

const CertificateController = require('../controllers/subject.controller');

// get all subjects
router.get('/subjects', SubjectCrudController.getAll);

// Add a subject
router.post('/subjects', SubjectCrudController.create);

// get a subject by id
router.get('/subjects/:id', SubjectCrudController.getOne);

// get saved subjects by user
router.get('/users/:userId/subjects', CertificateController.findAllFromUser);

// add subject to saved subjects
router.post('/users/:userId/subjects/:id', CertificateController.addToSavedSubjects);

// remove subject from saved subjects
router.delete('/users/:userId/subjects/:id', CertificateController.removeFromSavedSubjects);

module.exports = router