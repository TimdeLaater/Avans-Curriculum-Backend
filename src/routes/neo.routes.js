const express = require('express')
const NeoController = require('../controllers/neoController')
const router = express.Router()

NeoCrudController = new NeoController

// get all studyplans from user
router.get('/', function (req, res, next) {
    NeoCrudController.getAll(req, res, next);
})

router.get('/opleidingen', function (req, res, next) {
    NeoCrudController.getOpleidingen(req, res, next);
})
router.get('/OE', function (req, res, next) {
    NeoCrudController.getOnderwijsEenheid(req, res, next);
})

// add studyplan to user
router.post("/studyplans")

// delete studyplan from user
router.delete("/studyplans/:studyplanId")

// update studyplan from user
router.put("/studyplans/:studyplanId")

module.exports = router