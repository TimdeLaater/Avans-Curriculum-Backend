const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const getModel = require('./model_cache')

const StudyplanSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A Studyplan needs to have a name.']
    },
    cerficate: [{
        type: String,
        // required: [true, 'A Studyplan needs to have a certificate']
    }],
    details: {
        type: String,
    },
    modules: [{
        id: {
            type: Schema.Types.ObjectId,
            ref: 'Module',
            
        },
        completed: {
            type: Boolean,
            default: false
        },

    }],
    userId: {
        type: Schema.Types.ObjectId,
        required: [true, 'A Studyplan needs to have a user attached to it'],
        ref: 'User'

    },
    completed: {
        type: Boolean,
        default: false
    },
}
);




module.exports = getModel('Studyplan', StudyplanSchema)