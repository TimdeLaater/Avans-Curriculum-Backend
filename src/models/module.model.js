const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const getModel = require('./model_cache')

const ModuleSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A Module needs to have a name.']
    },
    details: {
        type: String,
        required: [true, 'A Module needs to have details']
    },
    subjects: [{
        type: Schema.Types.ObjectId,
        ref: 'Subject',
        required: [true, 'A Module needs to have at least one subject']
    }]
},
);




module.exports = getModel('Module', ModuleSchema)