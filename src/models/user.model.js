const mongoose = require('mongoose')
const Schema = mongoose.Schema

const getModel = require('./model_cache')

const UserSchema = new Schema({
    // a user needs to have a name
    name: {
        type: String,
        required: [true, 'A user needs to have a firstname.'],
    },

    // users email needs be a email
    //TODO: Add email validation
    email: {
        type: String,
        required: [true, 'A user needs to have a Emailadress'],
        unique: [true, 'A user needs to have a unique Emailadress'],
        validate: {
            validator: (email) => {
                const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return email.match(regex);
            },
        },
    },
    //TODO: Add birthday is in the past validation
    birthday: {
        type: Date,
        required: [true, 'A user needs to have a Birthday'],
        validate: {
            validator: (birthday) => {
                return birthday < Date.now();
            }
        }
    },
    password: {
        type: String,
        required: [true, 'A user needs to have a Password'],
    },
    certificateIds: [{
        type: Schema.Types.ObjectId,
        ref: 'Certificate'
    }],
    savedSubjectsIds: [{
        type: Schema.Types.ObjectId,
        ref: 'Subject',
    }],
    studyplanIds: [{
        type: Schema.Types.ObjectId,
        ref: 'Studyplan',
    }]

})


// export the user model through a caching function
module.exports = getModel('User', UserSchema)