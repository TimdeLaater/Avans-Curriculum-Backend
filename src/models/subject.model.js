const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const getModel = require('./model_cache')

const SubjectSchema = new Schema({
    name: {
        type: String,
        required: [true, 'A Subject needs to have a name.']
    },
    academyCodes: [{
        type: String,
    }],
    studyPoints: {
        type: Number,
        required: [true, 'A Subject needs to have a value for StudyPoints']
    },
    details: {
        type: String,
        required: [true, 'A Subject needs to have Details']
    },
    environment: {
        type: String,
        enum: ['online', 'physical', 'hybride'],
        required: [true, 'A Subject needs to have a Environment']
    },
}
);
// O-F-H



module.exports = getModel('Subject', SubjectSchema)