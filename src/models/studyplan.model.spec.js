const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Studyplan = require("./studyplan.model")()

describe('Studyplan model', function() {
    describe('unit tests', function() {
        it('should reject a missing name', async function() {
            const studyplan = new Studyplan({ userId: "61c44178b4a3d90023e21f42"})

            await expect(studyplan.save()).to.be.rejectedWith(Error)
        })
        it('should reject a missing UserID', async function() {
            const studyplan = new Studyplan({ name: "Technische Informatica"})

            await expect(studyplan.save()).to.be.rejectedWith(Error)
        })
        it('should create a studyplan', async function() {
            const test = new Studyplan({ name: "Technische Informatica", userId: "61c44178b4a3d90023e21f42" })

            await test.save()

            const studyplan = await Studyplan.findOne({ name: "Technische Informatica" })
            expect(studyplan).to.have.property('name', "Technische Informatica")
        })
    })
})