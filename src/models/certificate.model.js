const mongoose = require('mongoose')
const Schema = mongoose.Schema

const getModel = require('./model_cache')

const CertificateSchema = new Schema({
    // a certificate needs to have a name
    name: {
        type: String,
        required: [true, 'A Certificate needs to have a name.'],
    },

    academyCode: {
        type: String,
        required: [true, 'A Certificate needs to have a AcademyCode'],
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: [true, 'A user needs to be attached to a Certificate.'],
        ref: 'user'
    }
})


// export the user model through a caching function
module.exports = getModel('Certificate', CertificateSchema);