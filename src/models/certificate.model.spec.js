const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Certificate = require("./certificate.model")()

describe('Certificate model', function() {
    describe('unit tests', function() {
        it('should reject a missing name', async function() {
            const certificate = new Certificate({ academyCode: "I", userId: "61c44178b4a3d90023e21f42"})

            await expect(certificate.save()).to.be.rejectedWith(Error)
        })
        it('should reject a missing academyCode', async function() {
            const certificate = new Certificate({ name: "Informatica", userId: "61c44178b4a3d90023e21f42"})

            await expect(certificate.save()).to.be.rejectedWith(Error)
        })
        it('should reject a missing UserId', async function() {
            const certificate = new Certificate({ name: "Informatica", academyCode: "I"})

            await expect(certificate.save()).to.be.rejectedWith(Error)
        })
        it('should create a certificate', async function() {
            const test = new Certificate({ name: "Informatica", academyCode: "I", userId: "61c44178b4a3d90023e21f42" })

            await test.save()

            const cerficate = await Certificate.findOne({ name: "Informatica" })
            expect(cerficate).to.have.property('name', "Informatica")
        })
    })
})