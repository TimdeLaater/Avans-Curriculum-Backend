const chai = require('chai')
const expect = chai.expect

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')() // note we need to call the model caching function

describe('user model', function () {
    describe('unit tests', function () {
        it('should reject a missing user name', async function () {
            const user = new User({email: 'Joe.blackburn@gmail.com', password: 'joe', birthday: '2002-02-01'})

            await expect(user.save()).to.be.rejectedWith(Error)
        })


        it('should not create duplicate emails names', async function () {
            await new User({ email: 'Joe.blackburn@gmail.com', name: 'joe', password: 'joe', birthday: '2002-02-01' }).save()
            const user = new User({ email: 'Joe.blackburn@gmail.com', name: 'joe', password: 'joe', birthday: '2002-01-01' })

            await expect(user.save()).to.be.rejectedWith(Error)

            let count = await User.find().countDocuments()
            expect(count).to.equal(1)
        })
        it('should reject a missing user email', async function () {
            const user = new User({ name: "test" })

            await expect(user.save()).to.be.rejectedWith(Error)

        })
        it('should reject a missing user birthday', async function () {
            const user = new User({ name: "test", email: "Joe.blackburn@gmail.com" })

            await expect(user.save()).to.be.rejectedWith(Error)

        })
        
        it('should reject a missing user password', async function () {
            const user = new User({ name: "test", email: "Joe.blackburn@gmail.com", birthday: '2002-01-01' })

            await expect(user.save()).to.be.rejectedWith(Error)

        })
        it('should reject a user birthday in the future', async function () {
            const now = new Date();
            now.setDate(Date.now());
            
            let year = now.getFullYear();
            let month = now.getMonth();
            let day = now.getDay() + 1;

            let inputBirthday = `${year}-${month}-${day}`

            const user = new User({ name: "test", email: "Joe.blackburn@gmail.com", birthday: inputBirthday, password: "password" })

            await expect(user.save()).to.be.rejectedWith(Error)

        })
        it('should save the user', async function () {
            const test = new User({ name: "test", email: "Joe.blackburn@gmail.com", birthday: '2002-01-01', password: "password" })
            await test.save()
            const user = await User.findOne({ email: "Joe.blackburn@gmail.com" })
            expect(user).to.have.property('email', "Joe.blackburn@gmail.com")

        })
    })
})
