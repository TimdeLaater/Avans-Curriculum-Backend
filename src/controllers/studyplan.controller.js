const CrudController = require("../controllers/crud.js");
const StudyPlan = require("../models/studyplan.model")();
const User = require("../models/user.model")();

exports.create = async (req, res, next) => {
  const userId = req.params.userId;

  const studyplan = new StudyPlan({
    name: req.body.name,
    cerficate: req.body.cerficate,
    details: req.body.details,
    modules: req.body.modules,
    userId: req.params.userId,
  });

  User.findByIdAndUpdate(userId, {
    $push: { studyplanIds: studyplan._id },
  }).exec((err) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    studyplan
      .save(studyplan)
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Studyplan.",
        });
      });
  });
};

exports.findAllFromUser = (req, res) => {
  const userId = req.params.userId;
  User.findById(userId)
    .then((data) => {
      if (!data)
        res
          .status(400)
          .send({ message: `User with id ${userId} not found!` });
      else {
        const name = req.query.name;
        var condition = name
          ? { firstName: { $regex: new RegExp(name), $options: "i" } }
          : {};

        StudyPlan.find(condition)
          .then((data) => {
            res.send(data);
          })
          .catch((err) =>
            res.status(500).send({
              message:
                err.message || `Error occurred while retrieving Studyplans`,
            })
          );
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving Studyplans`,
      });
    });
};

exports.remove = (req, res) => {
  const userId = req.params.userId;
  const id = req.params.id;

  User.findByIdAndUpdate(userId, {
    $pull: { studyplanIds: id },
  }).exec((err) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else res.send({ message: `Successfully removed studyplan from user!` });
  });
};