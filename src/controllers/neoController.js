
const errors = require("../errors")
const neo = require('../../neo');

// the schema is supplied by injection
class NeoController {

    getAll = async (req, res, next) => {

        const session = neo.session();
        let result = '';


        try {
            result = await session.run(neo.getAll, {})
        } catch (error) {
            console.log('neo', error);
        }

        const count = result.records.map(i => i.get('data'))
        res.status(200).json(count);
    }
    getOpleidingen = async (req, res, next) => {
        const session = neo.session();
        let result = '';
        try {
            result = await session.run(neo.getOpleiding, {})
        } catch (error) {
            console.log('neo', error);
        }

        const count = result.records.map(i => i.get('data'))
        res.status(200).json(count);
    }
    getOnderwijsEenheid = async (req, res, next) => {
        const session = neo.session();
        let result = '';
        try {
            result = await session.run(neo.getOnderwijsEenheid, {})
        } catch (error) {
            console.log('neo', error);
        }

        const count = result.records.map(i => i.get('data'))
        res.status(200).json(count);
    }
}

module.exports = NeoController;