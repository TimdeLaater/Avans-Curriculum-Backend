const CrudController = require("../controllers/crud.js");
const Certificate = require("../models/certificate.model")();
const User = require("../models/user.model")();

exports.create = async (req, res, next) => {


  console.log(req.body);
  console.log(req.params.id);
  const user = await User.findById(req.params.userId);
  user.certificateIds.push(req.params.id);
  await user.save();
  res.status(201).json();

  // User.findByIdAndUpdate(userId, { $push: { certificates: CertificateCrudController.getOne } }).exec(
  //   (err) => {
  //     if (err) {
  //       res.status(500).send({ message: err });
  //       return;
  //     } else res.send({message: `Successfully added certificate to user!`})
  //   }
  // );
};

exports.findAllFromUser = (req, res) => {
  const userId = req.params.userId;
  User.findById(userId)
    .then((data) => {
      if (!data)
        res
          .status(400)
          .send({ message: `User with id ${userId} not found!` });
      else {
        const name = req.query.name;
        var condition = name
          ? { firstName: { $regex: new RegExp(name), $options: "i" } }
          : {};

        Certificate.find(condition)
          .then((data) => {
            res.send(data);
          })
          .catch((err) =>
            res.status(500).send({
              message:
                err.message || `Error occurred while retrieving certificates`,
            })
          );
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving Certificates`,
      });
    });
};
