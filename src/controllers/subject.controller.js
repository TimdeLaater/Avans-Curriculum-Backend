const CrudController = require("../controllers/crud.js");
const Subject = require("../models/certificate.model")();
const User = require("../models/user.model")();
const SubjectCrudController = new CrudController(Subject);

exports.findAllFromUser = (req, res) => {
  const userId = req.params.userId;

  User.findById(userId)
    .then((data) => {
      if (!data) res.status(400).send({ message: `User with id ${userId}` });
      else {
        const name = req.query.name;

        var condition = name
          ? { firstName: { $regex: new RegExp(name), $options: "i" } }
          : {};

        Subject.find(condition)
          .then((data) => {
            res.send(data);
          })
          .catch((err) =>
            res.status(500).send({
              message:
                err.message || `Error occurred while retrieving subjects`,
            })
          );
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error occurred while retrieving subjects`,
      });
    });
};

exports.addToSavedSubjects = (req, res) => {
  const userId = req.params.userId;
  const id = req.params.id;

  User.findByIdAndUpdate(userId, {
    $push: { savedSubjectsIds: id },
  }).exec((err) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else res.send({ message: `Successfully saved subject to user!` });
  });
};

exports.removeFromSavedSubjects = (req, res) => {
  const userId = req.params.userId;
  const id = req.params.id;

  User.findByIdAndUpdate(userId, {
    $pull: { savedSubjectsIds: id },
  }).exec((err) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    } else res.send({ message: `Successfully removed subject from user!` });
  });
};
